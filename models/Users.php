<?php

namespace app\models;

use DateTime;
use Exception;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $personal_code
 * @property string $phone
 * @property int $active
 * @property int $dead
 * @property string $lang
 * @property int $created_by
 * @property string $created_at
 * @property int $updated_by
 * @property string $updated_at
 *
 * @property Loan[] $loans
 */
class Users extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'personal_code', 'phone', 'lang', 'active', 'dead'], 'required'],
            [['active', 'dead'], 'integer'],
            [["email"], "email"],
            [['first_name', 'last_name', 'email', 'personal_code'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 20],
            [['lang'], 'string', 'max' => 4],
            [['personal_code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'personal_code' => 'Personal Code',
            'phone' => 'Phone',
            'active' => 'Active',
            'dead' => 'Dead',
            'lang' => 'Lang',

        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getLoans()
    {
        return $this->hasMany(Loan::className(), ['user_id' => 'id']);
    }

    /**
     * Get User age
     *
     * @param $personal_code
     * @return int User age
     * @throws Exception
     */
    public function getAge($personal_code): int
    {

        $date = new DateTime($this->getBirthDate($personal_code));
        $now = new DateTime();
        return $date->diff($now)->y;
    }

    /**
     * Get User birthday as Datetime object
     *
     * @param $personal_code
     * @return DateTime User birthday
     * @throws Exception
     */
    public function getBirthDate($personal_code)
    {
        $birthDate = "";
        if ($personal_code != "") {
            $year = substr($personal_code, 1, 2);
            $month = substr($personal_code, 3, 2);
            $day = substr($personal_code, 5, 2);
            $birthDate = new DateTime($year . '-' . $month . '-' . $day);
            $birthDate = date("Y-m-d", strtotime($birthDate->format('Y-m-d')));

        }
        return $birthDate;
    }
}
