<?php

/* @var $this View */
/* @var $content string */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://kit.fontawesome.com/99b63892af.js" crossorigin="anonymous"></script>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="main bg-black">
        <div class="container">
            <div class="flex flex-space-between notification-bar">
                <ul class="none flex flex-left">
                    <li>Klienditeenidus</li>
                    <li class="visible-md visible-lg"><i class="fas fa-mobile-alt"></i> 1715</li>
                    <li class="visible-md visible-lg"><i class="fas fa-clock"></i> E-P 9.00-21.00</li>
                </ul>
                <ul class="none flex flex-right">
                    <li class="visible-md visible-lg">Rwelamila, Denis Talemwa</li>
                    <li>
                        <button class="btn-oval btn-orange btn-shadow"><i class="fas fa-unlock"></i> &nbsp;&nbsp; Logout
                        </button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="bg-white">
            <div class="container">
                <?php echo Html::img('@web/images/cs_logo.svg', ['class' => 'float-left']) ?>
                <ol class="navigation nav nav-pills bg-transparent float-right">
                    <li class="nav-item"><?= Html::a("Users", ['users/index']) ?></li>
                    <li class="nav-item"><?= Html::a("Loans", ['loan/index']) ?></li>
                </ol>
            </div>
        </div>
        <div class="container-fluid bg-gray shadow-inset">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'options' => ['class' => 'container nav nav-pills'],
                'itemTemplate' => "<li class='nav-item'>{link}</li>",
                'activeItemTemplate' => "<li class='nav-item'><a href='#' class='nav-link active'>{link}</a></li>"
            ]) ?>
        </div>
    </div>

    <!--    --><?php
    //    NavBar::begin([
    //        'brandLabel' => 'My Company',
    //        'brandUrl' => Yii::$app->homeUrl,
    //        'options' => [
    //            'class' => 'navbar-inverse navbar-fixed-top',
    //        ],
    //    ]);
    //    echo Nav::widget([
    //        'options' => ['class' => 'navbar-nav navbar-right'],
    //        'items' => [
    //            ['label' => 'Home', 'url' => ['/site/index']],
    //            ['label' => 'About', 'url' => ['/site/about']],
    //            ['label' => 'Contact', 'url' => ['/site/contact']],
    //            Yii::$app->user->isGuest ? (
    //                ['label' => 'Login', 'url' => ['/site/login']]
    //            ) : (
    //                '<li>'
    //                . Html::beginForm(['/site/logout'], 'post')
    //                . Html::submitButton(
    //                    'Logout (' . Yii::$app->user->identity->username . ')',
    //                    ['class' => 'btn btn-link logout']
    //                )
    //                . Html::endForm()
    //                . '</li>'
    //            )
    //        ],
    //    ]);
    //    NavBar::end();
    //    ?>

    <div class="container">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>
<br>
<br>
<footer class="footer margin-top">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
