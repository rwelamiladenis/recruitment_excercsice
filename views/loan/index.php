<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LoanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Loans';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="container">
    <div class="bordered margin-top table-responsive">
        <h1 class="text-uppercase"><?php echo $this->title ?></h1>
        <table class="table table-striped">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'label' => 'Name',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->user['first_name'];
                        }
                    ],
                    'user_id',
                    'amount',
                    'interest',
                    'duration',
                    'start_date:date',
                    'campaign',
                    'status',
                    ['class' => 'yii\grid\ActionColumn', 'header' => 'Action'],
                ]

            ]) ?>
        </table>
    </div>

    <div class="margin-top">
        <?= Html::a('Create Loan', ['create'], ['class' => 'btn btn-oval btn-success btn-shadow']) ?>
    </div>
</div>
