<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div class="container">
    <div class="bordered margin-top">
        <h3 class="lead">Front-end style should be based on this page</h3>
        <p>Currently to be paid: <span>233.87 $</span> &nbsp;&nbsp;&nbsp; <span>233.87 $</span></p>
        <table class="table table-striped">
            <tr>
                <th>
                    Repayment Deadline
                </th>
                <th>
                    Loan Principle
                </th>
                <th>
                    Coast of Loan
                </th>
                <th>
                    Total according to schedule
                </th>
                <th>
                    Paid
                </th>
                <th>
                    To Pay
                </th>
            </tr>
            <tr>
                <td>28.11.2014</td>
                <td>53.94 $</td>
                <td>76.06 $</td>
                <td>130.00 $</td>
                <td>Paid</td>
                <td>133.51 $</td>
            </tr>
            <tr>
                <td>28.11.2014</td>
                <td>53.94 $</td>
                <td>76.06 $</td>
                <td>130.00 $</td>
                <td>Paid</td>
                <td>133.51 $</td>
            </tr>
            <tr>
                <td>28.11.2014</td>
                <td>53.94 $</td>
                <td>76.06 $</td>
                <td>130.00 $</td>
                <td>Paid</td>
                <td>133.51 $</td>
            </tr>
            <tr>
                <td>28.11.2014</td>
                <td>53.94 $</td>
                <td>76.06 $</td>
                <td>130.00 $</td>
                <td>Paid</td>
                <td>133.51 $</td>
            </tr>
            <tr>
                <td>28.11.2014</td>
                <td>53.94 $</td>
                <td>76.06 $</td>
                <td>130.00 $</td>
                <td>Paid</td>
                <td>133.51 $</td>
            </tr>
        </table>
    </div>

    <div class="margin-top">
        <button class="btn-oval btn-orange btn-shadow">Button Action 1</button>
        <button class="btn-oval btn-danger btn-shadow">Button Action 2</button>
        <button class="btn-oval btn-danger btn-shadow">Button Action 2</button>
    </div>
</div>
