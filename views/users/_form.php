<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-lg-6">
        <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'personal_code')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'active')->dropDownList(
            [0 => "No", 1 => "Yes"],
            ['prompt' => 'Select Status']); ?>
        <?= $form->field($model, 'dead')->dropDownList(
            [0 => "No", 1 => "Yes"],
            ['prompt' => 'Select Dead Status']); ?>
        <?= $form->field($model, 'lang')->dropDownList(
            ['est' => "est", "rus" => "rus"],
            ['prompt' => 'Select Language']); ?>
    </div>
    <div class="col-ls-12">
        <div class="form-group pull-right">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app', 'Close'), ['index'], ['class' => 'btn btn-warning']); ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
