<?php

use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = "User Detail";
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="users-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p class="pull-right">
        <?= Html::a('<i class="fa fa-pencil"></i> Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //  'id',
            'first_name',
            'last_name',
            [
                'label' => 'Date Birth',
                'value' => $model->getBirthDate($model->personal_code),
            ],
            [

                'label' => 'Age ',
                'value' => $model->getAge($model->personal_code) . " Years",
            ],
            'email:email',
            'personal_code',
            'phone',
            // 'active',
            [

                'attribute' => 'active',
                'value' => $model->active == 1 ? "Yes" : "No",
            ],
            // 'dead',
            [

                'attribute' => 'dead',
                'value' => $model->dead == 1 ? "Yes" : "No",
            ],
            'lang',
            // 'created_by',
            //'created_at',
            //  'updated_by',
            // 'updated_at',
        ],
    ]) ?>

</div>
