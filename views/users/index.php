<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="container">
    <div class="bordered margin-top table-responsive">
        <h1 class="text-uppercase"><?php echo $this->title ?></h1>
        <table class="table table-striped">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'first_name',
                    'last_name',
                    'email:email',
                    'personal_code',
                    'phone',
                    'active',
                    [

                        'attribute' => 'dead',
                        'value' => function ($model) {
                            return $model->dead == 1 ? "Yes" : "No";
                        }
                    ],
                    'lang',
                    ['class' => 'yii\grid\ActionColumn', 'header' => 'Action'],
                ]

            ]) ?>
        </table>
    </div>

    <div class="margin-top">
        <?= Html::a('Create Users', ['create'], ['class' => 'btn btn-oval btn-success btn-shadow']) ?>
    </div>
</div>

