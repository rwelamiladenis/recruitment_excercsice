<?php

use yii\db\Connection;

return [
    'class' => Connection::class,
    'dsn' => 'pgsql:host=db;dbname=example',
    'username' => 'example',
    'password' => 'example',
    'charset' => 'utf8'
];
