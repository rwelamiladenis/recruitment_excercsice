<?php


namespace app\commands;


use app\models\Loan;
use app\models\Users;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class ImportController extends Controller
{
    public function actionUsers()
    {
        $path = Yii::$app->basePath . "/users.json";
        if (file_exists($path)) {
            $array = json_decode(file_get_contents($path), true);
            foreach ($array as $data) {
                $user = new Users();
                $user->id = isset($data['id']) ? $data['id'] : null;
                $user->first_name = isset($data['first_name']) ? $data['first_name'] : null;
                $user->last_name = isset($data['last_name']) ? $data['last_name'] : null;
                $user->email = isset($data['email']) ? $data['email'] : null;
                $user->personal_code = isset($data['personal_code']) ? $data['personal_code'] : null;
                $user->phone = isset($data['phone']) ? $data['phone'] : null;
                $user->active = isset($data['active']) ? $data['active'] : null;
                $user->dead = isset($data['dead']) ? $data['dead'] : null;
                $user->lang = isset($data['lang']) ? $data['lang'] : null;
                $user->save();
            }
        } else {
            echo "File not found!\n";
            return ExitCode::UNAVAILABLE;
        }
        return ExitCode::OK;
    }

    public function actionLoans()
    {
        $path = Yii::$app->basePath . "/loans.json";
        if (file_exists($path)) {
            $array = json_decode(file_get_contents($path), true);
            foreach ($array as $data) {
                $loan = new Loan();
                $loan->id = isset($data['id']) ? $data['id'] : null;
                $loan->user_id = isset($data['user_id']) ? $data['user_id'] : null;
                $loan->amount = isset($data['amount']) ? $data['amount'] : null;
                $loan->interest = isset($data['interest']) ? $data['interest'] : null;
                $loan->duration = isset($data['duration']) ? $data['duration'] : null;
                $loan->start_date = isset($data['start_date']) ? $data['start_date'] : null;
                $loan->end_date = isset($data['end_date']) ? $data['end_date'] : null;
                $loan->campaign = isset($data['campaign']) ? $data['campaign'] : null;
                $loan->status = isset($data['status']) ? $data['status'] : null;
                $loan->save();

            }
        } else {
            echo "File not found!\n";
            return ExitCode::UNAVAILABLE;
        }
        return ExitCode::OK;
    }

}